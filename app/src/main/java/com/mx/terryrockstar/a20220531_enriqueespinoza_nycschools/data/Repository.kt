package com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.data

import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.data.source.remote.SchoolService
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.model.Sat
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.model.School

class Repository {

    private val api = SchoolService()

    suspend fun getSchools(): List<School> {
        return api.getSchools()
    }

    suspend fun getSat(dbn: String): Sat {
        return api.getSat(dbn)
    }

}