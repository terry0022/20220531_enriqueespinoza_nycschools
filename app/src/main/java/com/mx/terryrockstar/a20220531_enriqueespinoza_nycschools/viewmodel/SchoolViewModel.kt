package com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.data.Repository
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.domain.GetSatUseCase
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.domain.GetSchoolUseCase
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.model.Sat
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.model.School
import kotlinx.coroutines.launch

class SchoolViewModel : ViewModel() {

    private val _items = MutableLiveData<List<School>>().apply { value = emptyList() }
    val items: LiveData<List<School>> = _items

    private val _isLoading = MutableLiveData<Boolean>().apply { value = false }
    val isLoading: LiveData<Boolean> = _isLoading

    private val _nodata = MutableLiveData<Boolean>().apply { value = false }
    val nodata: LiveData<Boolean> = _nodata

    private val _sat = MutableLiveData<Sat>()
    val sat: LiveData<Sat> = _sat

    private val repository = Repository()
    private var getSchoolUseCase = GetSchoolUseCase(repository)
    private var getSatUseCase = GetSatUseCase(repository)

    fun getSchools() = viewModelScope.launch {
        val result = getSchoolUseCase()

        if (result.isNotEmpty()) {
            _items.value = result
        }

    }

    fun getSat(dbn: String) = viewModelScope.launch {
        val result = getSatUseCase(dbn)

        if (!result.dbn.isNullOrEmpty()) {
            _sat.value = result
        } else {
            _nodata.value = true
        }
    }
}