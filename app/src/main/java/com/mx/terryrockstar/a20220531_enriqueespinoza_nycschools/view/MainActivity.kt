package com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.R
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.databinding.ActivityMainBinding
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.databinding.DialogSatBinding
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.model.Sat
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.viewmodel.SchoolViewModel

class MainActivity : AppCompatActivity() {


    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

    private val schoolViewModel: SchoolViewModel by viewModels()

    private lateinit var schoolAdapter: SchoolAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater).apply {
            this.viewmodel = schoolViewModel
        }
        setContentView(binding.root)
        setupProgressbar()
        setupSchoolAdapter()
        setupShowSchools()
        setupSat()
    }

    override fun onResume() {
        super.onResume()
        schoolViewModel.getSchools()
    }

    private fun setupSat() {
        schoolViewModel.sat.observe(this) { sat ->
            dialog(sat).show()
        }
        schoolViewModel.nodata.observe(this) {
            if (it) {
                Toast.makeText(this, "No data available", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun setupProgressbar() {
        schoolViewModel.isLoading.observe(this) {
            binding.recycler.isVisible = !it
            binding.progressCircular.isVisible = it
        }
    }

    private fun setupShowSchools() {
        schoolViewModel.items.observe(this) { list ->
            schoolAdapter.submitList(list)
        }
    }

    private fun setupSchoolAdapter() {
        val viewModel = binding.viewmodel
        if (viewModel != null) {
            schoolAdapter = SchoolAdapter() { dbn ->
                schoolViewModel.getSat(dbn)
            }
            binding.recycler.adapter = schoolAdapter
        }
    }

    private fun dialog(sat: Sat): AlertDialog {
        val alertBinding = DialogSatBinding.inflate(layoutInflater).apply {
            this.sat = sat
        }
        val builder = AlertDialog.Builder(this).setView(alertBinding.root)
            .setCancelable(true)
            .setPositiveButton(R.string.close) { dialog, _ ->
                dialog.dismiss()
            }
        return builder.create()
    }

}