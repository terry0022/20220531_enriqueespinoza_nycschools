package com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.view

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.databinding.ItemSchoolBinding
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.model.School

class SchoolAdapter(private val listener: (String) -> Unit) :
        ListAdapter<School, SchoolAdapter.ViewHolder>(SchoolDiffCallback()) {

    class ViewHolder private constructor(private val binding: ItemSchoolBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: School, listener: (String) -> Unit) {
            Log.d("SchoolAdapter", "item: $item")
            binding.school = item
            binding.executePendingBindings()

            binding.button.setOnClickListener {
                listener(item.dbn)
            }
        }

        companion object {
            fun from(parent: ViewGroup) : ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemSchoolBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, listener)
    }
}

class SchoolDiffCallback: DiffUtil.ItemCallback<School>() {
    override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
        return oldItem.dbn == newItem.dbn
    }

    override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
        return oldItem == newItem
    }

}