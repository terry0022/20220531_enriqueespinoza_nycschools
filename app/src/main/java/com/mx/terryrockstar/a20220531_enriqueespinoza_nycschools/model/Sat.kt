package com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.model

import android.os.Parcelable
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Sat(
    @SerializedName("dbn") val dbn: String?,
    @SerializedName("school_name") val name: String?,
    @SerializedName("num_of_sat_test_takers") val testTaker: Int,
    @SerializedName("sat_critical_reading_avg_score") val readingAvg: Int,
    @SerializedName("sat_math_avg_score") val mathAvg: Int,
    @SerializedName("sat_writing_avg_score") val writingAvg: Int
) : Parcelable {
    constructor() : this(null, null,-1, -1, -1, -1)

    override fun toString(): String {
        return Gson().toJson(this)
    }

    fun getReading() : String {
        return "Reading: $readingAvg"
    }

    fun getMath() : String {
        return "Math: $mathAvg"
    }

    fun getWriting() : String {
        return "Writing: $writingAvg"
    }

    fun getAverage() : String {
        val average = (readingAvg + mathAvg + writingAvg) / 3
        return "$average"
    }
}
