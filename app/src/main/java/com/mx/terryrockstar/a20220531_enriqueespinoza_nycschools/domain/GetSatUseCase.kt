package com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.domain

import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.data.Repository
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.model.Sat

class GetSatUseCase(private val repository: Repository) {

    suspend operator fun invoke(dbn: String): Sat {
        return repository.getSat(dbn)
    }

}