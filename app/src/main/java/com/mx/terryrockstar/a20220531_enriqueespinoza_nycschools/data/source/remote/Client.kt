package com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.data.source.remote

import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.model.Sat
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.model.School
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface Client {

    @GET("s3k6-pzi2.json")
    suspend fun getSchools(): Response<List<School>>

    @GET("f9bf-2cp4.json")
    suspend fun getSat(@Query("dbn") dbn: String): Response<List<Sat>>

}