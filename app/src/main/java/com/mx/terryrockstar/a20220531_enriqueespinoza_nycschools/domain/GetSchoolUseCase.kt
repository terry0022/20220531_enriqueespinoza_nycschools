package com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.domain

import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.data.Repository
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.model.School

class GetSchoolUseCase(private val repository: Repository) {

    suspend operator fun invoke(): List<School> {
        return repository.getSchools()
    }

}