package com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.model

import android.os.Parcelable
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class School(
    @SerializedName("dbn") val dbn: String,
    @SerializedName("school_name") val schoolName: String,
    @SerializedName("school_email") val website: String?
) : Parcelable {
    override fun toString(): String {
        return Gson().toJson(this)
    }
}
