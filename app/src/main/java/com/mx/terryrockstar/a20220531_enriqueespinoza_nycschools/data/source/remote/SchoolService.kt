package com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.data.source.remote

import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.data.source.RetrofitHelper
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.model.Sat
import com.mx.terryrockstar.a20220531_enriqueespinoza_nycschools.model.School
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SchoolService {

    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
    private val client = RetrofitHelper.getRetrofit().create(Client::class.java)

    suspend fun getSchools(): List<School> {
        return withContext(dispatcher) {
            val response = client.getSchools()
            return@withContext response.body() ?: emptyList()
        }
    }

    suspend fun getSat(dbn: String): Sat {
        return withContext(dispatcher) {
            val response = client.getSat(dbn)
            return@withContext if (response.body().isNullOrEmpty()) {
                Sat()
            } else {
                response.body()!![0]
            }
        }
    }

}